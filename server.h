#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/evp.h>

class QTcpServer;
class QTcpSocket;

class Server : public QObject
{
    Q_OBJECT

private:
    /***************************** SERVER *****************************/

    QTcpServer *_tcpServer;
    QTcpSocket *_clientSocket;
    quint16 _nextBlockSize;
    QList<QTcpSocket*> _clients; // список клиентских сокетов
    //QTextEdit *_text;

    int numbOfSocket = -1;

    void sendToClient(QTcpSocket *socket, const QString &str);

    /******************************************************************/


    /**************************** DATABASE ****************************/

    QSqlQuery *query;

    bool connectionToDatabase();
    bool authorization(QString log, QString pass);
    void show(QString table);
    void find(QString find);
    void add(QString add);
    void deleteRow(QString del);
    void change(QString change);

    /******************************************************************/

public: //SERVER
    Server(quint16 port);
    void ReadAuth(QString isUserAuth);

public slots: // SERVER
    virtual void slotNewConnection();
    void slotReadClient();
};

#endif // SERVER_H
