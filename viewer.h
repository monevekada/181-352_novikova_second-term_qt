#ifndef VIEWER_H
#define VIEWER_H

#include <QObject>
#include <QTableView>

namespace Ui {
    class Viewer;
}

class Viewer : public QObject
{
    Q_OBJECT
public:
    explicit Viewer(QObject *parent = nullptr);
    void createTable(QString show, QString type, QTableView *table);

signals:

public slots:
};

#endif // VIEWER_H
