#ifndef ADMIN_H
#define ADMIN_H

#include <QDialog>
#include <QTcpSocket>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/evp.h>
#include "viewer.h"

namespace Ui {
class Admin;
}

class Admin : public QDialog
{
    Q_OBJECT

private:
    Ui::Admin *ui;
    Viewer *view;

    QTcpSocket *_tcpSocket;
    quint64 _nextBlockSize;

    QString encrypt(QString plainText, int do_encrypt); // 0 - decrypt, 1 - encrypt

public:
    Admin(QTcpSocket *mainSocket, qintptr descriptor);
    ~Admin();

signals:

public slots:
    void slotReadyRead();
    void slotSendToServer(const QString &str);

private slots:
    void on_pushButtonFind_clicked();
    void on_pushButtonU_clicked();
    void on_pushButtonSR_clicked();
    void on_pushButtonDelete_clicked();
    void on_pushButtonAdd_clicked();
    void on_pushButtonChange_clicked();
};

#endif // ADMIN_H
