#include "user.h"
#include "ui_user.h"
#include <QMessageBox>

User::User(QTcpSocket *mainSocket, qintptr descriptor) :
    ui(new Ui::User)
{
    ui->setupUi(this);
    _nextBlockSize = 0;
    _tcpSocket = new QTcpSocket;
    _tcpSocket->setSocketDescriptor(descriptor);

    connect(_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
}

User::~User()
{
    delete ui;
}

void User::slotReadyRead()
{
    QDataStream in(_tcpSocket);

    while(true){
        if (_nextBlockSize == 0){
            if (_tcpSocket->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                break;
            }
            in >> _nextBlockSize;
        }

        if (_tcpSocket->bytesAvailable() < _nextBlockSize){
            break;
        }

        QString str;
        in >> str;

        if (str == "show:failed" || str == "find:failed") {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");

            msgBox.setText("Error!");
            msgBox.setInformativeText("Something came wrong! Please try again.");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);

            if(msgBox.exec() == QMessageBox::Ok)
            {
                msgBox.close();
            }
        }

        else if (str.contains("show:")) {
            if (str.contains(":sales:")) {
            view->createTable(str, "sales", ui->tableView);
            }
        }


        _nextBlockSize = 0;
    }
}

void User::slotSendToServer(const QString &str)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint16(0) << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    _tcpSocket->write(arrBlock);
}

void User::on_pushButtonSR_clicked()
{
    slotSendToServer("show:sales");
}

void User::on_pushButtonFind_clicked()
{
    QString find = ui->lineEditFind->text();
    ui->lineEditFind->clear();
    int temp = 0, count = 0;

    while(count < find.count(" ")) {
    temp = find.indexOf(" ", temp);
    find.replace(temp, 1, ":");
    temp++;
    }
    find.insert(0, "find:");

    slotSendToServer(find);
}
