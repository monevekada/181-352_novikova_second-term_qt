#include "viewer.h"
#include <QStandardItemModel>

Viewer::Viewer(QObject *parent) : QObject(parent)
{
}

void Viewer::createTable(QString show, QString type, QTableView *table)
{
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::ExtendedSelection);
    int tempRow = 0;
    QString activeRow;
    tempRow = show.count(";");

    if (type == "users") {
    show.remove(show.indexOf("show:users:"), 11);

    QStandardItemModel *model = new QStandardItemModel(tempRow, 4);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("LOGIN")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("PASSWORD")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("ACCESS")));

    for(int i = 0; i < tempRow; i++){
        activeRow = show.section(";", i, i);
        for(int j = 0; j < 4; j++){
            QStandardItem *item = new QStandardItem(QString(activeRow.section(":", j, j)));
            model->setItem(i, j, item);
        }
    }
    table->setModel(model);
    }

    if (type == "sales") {
    show.remove(show.indexOf("show:sales:"), 11);

    QStandardItemModel *model = new QStandardItemModel(tempRow, 6);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("TITLE")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("AUTHOR")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("SELLER")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("BUYER")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("DATE")));

    for(int i = 0; i < tempRow; i++){
        activeRow = show.section(";", i, i);
        for(int j = 0; j < 6; j++){
            QStandardItem *item = new QStandardItem(QString(activeRow.section(":", j, j)));
            model->setItem(i, j, item);
        }
    }
    table->setModel(model);
    }
}
