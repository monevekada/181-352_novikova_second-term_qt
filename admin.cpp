#include "admin.h"
#include "ui_admin.h"
#include <QDate>
#include <QMessageBox>
#include <QStandardItemModel>

Admin::Admin(QTcpSocket *mainSocket, qintptr descriptor) :
    ui(new Ui::Admin)
{
    ui->setupUi(this);    
    _nextBlockSize = 0;
    _tcpSocket = new QTcpSocket;
    _tcpSocket->setSocketDescriptor(descriptor);

    connect(_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    //encrypt(encrypt("wwwww", 1), 0);
}

Admin::~Admin()
{
    delete ui;
}

void Admin::slotReadyRead()
{
    QDataStream in(_tcpSocket);

    while(true){
        if (_nextBlockSize == 0){
            if (_tcpSocket->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                break;
            }
            in >> _nextBlockSize;
        }

        if (_tcpSocket->bytesAvailable() < _nextBlockSize){
            break;
        }

        QString str;
        in >> str;

        if (str == "show:failed" || str == "find:failed" ||
                str == "add:failed" || str == "delete:failed" ||
                str == "change:failed") {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");

            msgBox.setText("Error!");
            msgBox.setInformativeText("Something came wrong! Please try again.");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);

            if(msgBox.exec() == QMessageBox::Ok)
            {
                msgBox.close();
            }
        }

        else if (str.contains("show:")) {
            if (str.contains(":users:")) {
            view->createTable(str, "users", ui->tableView);
            }
            if (str.contains(":sales:")) {
            view->createTable(str, "sales", ui->tableView);
            }
        }


        _nextBlockSize = 0;
    }
}

void Admin::slotSendToServer(const QString &str)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint16(0) << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    _tcpSocket->write(arrBlock);
}

QString Admin::encrypt(QString plainText, int do_encrypt)
{
        unsigned char inbuf[1024], outbuf[1024 + EVP_MAX_BLOCK_LENGTH];
        int inlen, outlen;
        QByteArray encryptedText;

        unsigned char *sourseBinaryText, *outSourseBinaryText;
        unsigned char key[] = "0123456789abcdeF0123456789abcdeF"; //16x8 = 128 (бит)
        unsigned char iv[] = "1234567887654321"; //случайное число (по длине ключа)

        EVP_CIPHER_CTX *ctx; // структура для указания алгоритма шифрования и его параметров
        ctx = EVP_CIPHER_CTX_new();

        EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), nullptr, nullptr, nullptr, do_encrypt);
        OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 32);
        OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);

        EVP_CipherInit_ex(ctx, nullptr, nullptr, key, iv, do_encrypt);

        //EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv, do_encrypt);

            QByteArray byte = plainText.toUtf8();
            sourseBinaryText = (unsigned char*)byte.data();

            int k = 0;
            while(!(k == strlen((char*)sourseBinaryText - 1))) {
                for(int i = 0; i < 1024; i++) {
                    inbuf[1024] = sourseBinaryText[i];
                }
                    if(!EVP_CipherUpdate(ctx, outbuf, &outlen, inbuf, 1024)){ // шифрование или дешифрование (зависит от do_encrypt)
                        /* Error */
                        EVP_CIPHER_CTX_free(ctx);
                        return "";
                    }
                outSourseBinaryText[1024+k] = outbuf[1024];
                k+=1024;
        }


        //if(!EVP_CipherFinal_ex(ctx, outbuf, &outlen)){ // Возвращает результат шифрования/дешифрования последнего неполного блока, если принудительно оключен режим финализации
            /* Error */
            /*EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        fwrite(outbuf, 1, outlen, out);*/

        EVP_CIPHER_CTX_free(ctx); // удаление ctx

        //(unsigned char*)encryptedText.data() = outSourseBinaryText;
        //encryptedText =
          return      QString::fromUtf8((char*)outSourseBinaryText,strlen((char*)outSourseBinaryText)-1);
        //strcpy((unsigned char*)encryptedText.data(), outSourseBinaryText);
        //return QString::fromUtf8(encryptedText);
}


void Admin::on_pushButtonU_clicked()
{
    slotSendToServer("show:users");
}


void Admin::on_pushButtonSR_clicked()
{
    slotSendToServer("show:sales");
}


void Admin::on_pushButtonFind_clicked()
{
    QString find = ui->lineEditFind->text();
    ui->lineEditFind->clear();
    int temp = 0, count = 0;

    while(count < find.count(" ")) {
    temp = find.indexOf(" ", temp);
    find.replace(temp, 1, ":");
    temp++;
    }
    find.insert(0, "find:");

    slotSendToServer(find);
}

void Admin::on_pushButtonDelete_clicked()
{
    QString del = ui->lineEditIDChange->text();
    del.insert(0, "delete:");
    slotSendToServer(del);
    ui->lineEditIDChange->clear();
    slotSendToServer("show:sales");
}

void Admin::on_pushButtonAdd_clicked()
{
    QString add = "add:";
    add.append(ui->lineEditTitle->text() + ":" +
                ui->lineEditAuthor->text() + ":" +
                ui->lineEditBuyer->text() + ":" +
                ui->lineEditSeller->text() + ":");
    add.append(QDate::currentDate().toString("yyyy-MM-dd"));

    slotSendToServer(add);

    ui->lineEditTitle->clear();
    ui->lineEditAuthor->clear();
    ui->lineEditBuyer->clear();
    ui->lineEditSeller->clear();

    slotSendToServer("show:sales");
}

void Admin::on_pushButtonChange_clicked()
{
    QString change = "change:";
    change.append(ui->lineEditIDChange->text() + ":" +
                ui->lineEditTitleChange->text() + ":" +
                ui->lineEditAuthorChange->text() + ":" +
                ui->lineEditBuyerChange->text() + ":" +
                ui->lineEditSellerChange->text());

    slotSendToServer(change);

    ui->lineEditIDChange->clear();
    ui->lineEditTitleChange->clear();
    ui->lineEditAuthorChange->clear();
    ui->lineEditBuyerChange->clear();
    ui->lineEditSellerChange->clear();

    slotSendToServer("show:sales");
}
