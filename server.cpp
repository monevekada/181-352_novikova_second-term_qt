#include "server.h"
#include "admin.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>
#include <QCryptographicHash>
#include <QSqlRecord>
//#include <QTime>


/***************************** SERVER *****************************/

Server::Server(quint16 port)
{
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }
    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
}

void Server::slotNewConnection(){
    QTcpSocket *_clientSocket;
    _clientSocket = _tcpServer->nextPendingConnection();
    _clients << _clientSocket;

    connect(_clientSocket, SIGNAL(disconnected()), _clientSocket, SLOT(deleteLater()));
    connect(_clientSocket, SIGNAL(readyRead()), this, SLOT(slotReadClient()));
}

void Server::slotReadClient(){
    QTcpSocket *_clientSocket = qobject_cast<QTcpSocket *>(sender());
    if (_clientSocket == 0) return;
    QDataStream in(_clientSocket);

    while(true){
        if (_nextBlockSize == 0){
            if (_clientSocket->bytesAvailable() < static_cast<int>(sizeof(quint16))){
                break;
            }
            in >> _nextBlockSize;
        }

        if (_clientSocket->bytesAvailable() < _nextBlockSize){
            break;
        }

        QString str;
        in >> str;

        if (str.contains("auth:")) {
            for (int i = 0; i < _clients.size(); i++) {
                if (_clients[i] == _clientSocket) {
                    numbOfSocket = i;
                }
            }
            QString::SectionFlag flag = QString::SectionSkipEmpty;

            QString login = str.section(":", 1, 1);
            QString password = str.section(":", 2, 2, flag);

            authorization(login, password);
        }

         if (str.contains("show:")) {
             if (str.contains("users")) {
                 show("users");
             }
             if (str.contains("sales")) {
                 show("sales");
             }
         }
         if (str.contains("find:")) {
            find(str);
         }
         if (str.contains("add:")) {
             add(str);
         }
         if (str.contains("change:")) {
             change(str);
         }
         if (str.contains("delete:")) {
             deleteRow(str);
         }

        //QString message = time.toString() + " " + str;
        //_text->append(message);

        _nextBlockSize = 0;
    }
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint64(0) << str;
    out.device()->seek(0);
    out << quint64(arrBlock.size() - static_cast<int>(sizeof(quint64)));

    socket->write(arrBlock);
}

void Server::ReadAuth(QString isUserAuth)
{
    if (isUserAuth == "NotOk") {
        sendToClient(_clients[numbOfSocket], "auth:failed");
    }
    else {
        if (isUserAuth == "admin") {
            sendToClient(_clients[numbOfSocket], "auth:admin");
        }
        else if (isUserAuth == "user") {
            sendToClient(_clients[numbOfSocket], "auth:user");
        }
    }
}

/******************************************************************/





/**************************** DATABASE ****************************/

bool Server::connectionToDatabase()
{
    /*QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setDatabaseName("n92547nr_test.mysql");
    db.setUserName("n92547nr");
    // https://free2.beget.com/phpMyAdmin/server_databases.php?token=75f6b91177f3282f11811d1760746b17
    db.setHostName("185.50.25.36");
    db.setPassword("XsldXFnl");*/

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("F:\\Polytech\\Programming\\QT\\181_352_Novikova_Database\\database_1");
    query = new QSqlQuery(db);
    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError().text();
        return false;
    }

    return true;
}

bool Server::authorization(QString log, QString pass)
{
    if (!connectionToDatabase()) {
        return -1;
    }

    QString passHash = QCryptographicHash::hash(pass.toUtf8(), QCryptographicHash::Sha1).toHex(), str;

    QSqlRecord rec = query->record();
    QString login;
    QString password;
    QString access;

    str = "SELECT * FROM users WHERE login = '%1';";
    QString strF = str.arg(log);
    if (!query->exec(strF)) {
        qDebug() << "Unable to find login";
    }
    else {
        QSqlRecord rec = query->record();
        while (query->next()) {
        login = query->value(rec.indexOf("login")).toString();
        password = query->value(rec.indexOf("password")).toString();
        access = query->value(rec.indexOf("access")).toString();
        }

        if (log == login && passHash == password) {
            if (access == "admin") {
                 ReadAuth("admin");
            }
            else if (access == "user"){
                 ReadAuth("user");
            }
        }
        else {
            ReadAuth("NotOk");
        }
    }
    return true;
 }

void Server::show(QString table)
{
    QString str;
    if (table == "users") {
        str = "SELECT * FROM users;";
    }
    if (table == "sales") {
        str = "SELECT * FROM sales;";
    }

    QString show;
    if (!query->exec(str)) {
        qDebug() << "Unable to SELECT * FROM users;";
        _clientSocket->write("show:failed");
    }

    else if (table == "users") {
        show = "show:users:";
        QString id;
        QString login;
        QString password;
        QString access;

        QSqlRecord rec = query->record();
        while (query->next()) {
        id = query->value(rec.indexOf("number")).toString();
        login = query->value(rec.indexOf("login")).toString();
        password = query->value(rec.indexOf("password")).toString();
        access = query->value(rec.indexOf("access")).toString();
        show.append(id + ":" + login + ":" + password + ":" + access + ";");
        }
    }

    else if (table == "sales") {
        show = "show:sales:";
        QString id;
        QString title;
        QString author;
        QString buyer;
        QString seller;
        QString date;

        QSqlRecord rec = query->record();
        while (query->next()) {
        id = query->value(rec.indexOf("number")).toString();
        title = query->value(rec.indexOf("title")).toString();
        author = query->value(rec.indexOf("author")).toString();
        buyer = query->value(rec.indexOf("buyer")).toString();
        seller = query->value(rec.indexOf("seller")).toString();
        date = query->value(rec.indexOf("date")).toString();
        show.append(id + ":" + title + ":" + author + ":" + buyer + ":" + seller + ":" + date + ";");
        }
    }
    sendToClient(_clients[numbOfSocket], show);
}


void Server::find(QString find)
{
    find = find.remove(find.indexOf("find:"), 5);
    int temp = 0, count = 0;
    while(count < find.count(":")) {
    temp = find.indexOf(":", temp);
    find.replace(temp, 1, " AND ");
    temp++;
    }

    QString str = QString ("SELECT * FROM sales WHERE title LIKE '%%1%' OR author LIKE '%%1%' OR seller LIKE '%%1%' OR buyer LIKE '%%1%' OR date LIKE '%%1%';").arg(find);
    QString show;

    if(!query->exec(str)) {
        sendToClient(_clients[numbOfSocket], "find:failed");
    }
    else {
        QString id;
        QString title;
        QString author;
        QString buyer;
        QString seller;
        QString date;

        QSqlRecord rec = query->record();
        while (query->next()) {
            id = query->value(rec.indexOf("number")).toString();
            title = query->value(rec.indexOf("title")).toString();
            author = query->value(rec.indexOf("author")).toString();
            buyer = query->value(rec.indexOf("buyer")).toString();
            seller = query->value(rec.indexOf("seller")).toString();
            date = query->value(rec.indexOf("date")).toString();
            show.append(id + ":" + title + ":" + author + ":" + buyer + ":" + seller + ":" + date + ";");
        }
        show.insert(0, "show:sales:");
        sendToClient(_clients[numbOfSocket], show);
    }
}

void Server::add(QString add)
{
    add = add.remove(add.indexOf("add:"), 4);
    QString str = QString ("INSERT INTO sales ( title, author, buyer, seller, date ) VALUES ( '%1', '%2', '%3', '%4', '%5' );").
            arg(add.section(":", 0, 0)).
            arg(add.section(":", 1, 1)).
            arg(add.section(":", 2, 2)).
            arg(add.section(":", 3, 3)).
            arg(add.section(":", 4, 4));

    if(!query->exec(str)) {
        sendToClient(_clients[numbOfSocket], "add:failed");
    }
}

void Server::change(QString change)
{
    change = change.remove(change.indexOf("change:"), 7);
    QString str;

    if (change.section(":", 1, 1) != "") {
        str = QString("UPDATE sales SET title = '%1' WHERE number = '%2';").
                arg(change.section(":", 1, 1)).
                arg(change.section(":", 0, 0));
    }
    if (change.section(":", 2, 2) != "") {
        str = QString("UPDATE sales SET author = '%1' WHERE number = '%2';").
                arg(change.section(":", 2, 2)).
                arg(change.section(":", 0, 0));
    }
    if (change.section(":", 3, 3) != "") {
        str = QString("UPDATE sales SET buyer = '%1' WHERE number = '%2';").
                arg(change.section(":", 3, 3)).
                arg(change.section(":", 0, 0));
    }
    if (change.section(":", 4, 4) != "") {
        str = QString("UPDATE sales SET seller = '%1' WHERE number = '%2';").
                arg(change.section(":", 4, 4)).
                arg(change.section(":", 0, 0));
    }

    if(!query->exec(str)) {
        sendToClient(_clients[numbOfSocket], "change:failed");
    }
}

void Server::deleteRow(QString del)
{
    del = del.remove(del.indexOf("delete:"), 7);
    QString str = QString ("DELETE FROM sales WHERE number = '%1';").arg(del);

    if(!query->exec(str)) {
        sendToClient(_clients[numbOfSocket], "delete:failed");
    }
}
/******************************************************************/
