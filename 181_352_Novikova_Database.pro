#-------------------------------------------------
#
# Project created by QtCreator 2019-05-06T16:19:51
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 181_352_Novikova_Database
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    admin.cpp \
        main.cpp \
        mainwindow.cpp \
    server.cpp \
    user.cpp \
    viewer.cpp

HEADERS += \
    admin.h \
        mainwindow.h \
    server.h \
    user.h \
    viewer.h

FORMS += \
        admin.ui \
        mainwindow.ui \
        user.ui

INCLUDEPATH += D:/OpenSSL-Win64/include
LIBS += -LD:/OpenSSL-Win64/
LIBS += -LD:/OpenSSL-Win64/lib -llibssl
LIBS += -LD:/OpenSSL-Win64/lib -llibcrypto

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
