#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/evp.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void authorizationFailed();

public slots:
    void slotReadyRead();
    void slotSendToServer();

private slots:
    void on_pushButtonOK_clicked();

private:
    Ui::MainWindow *ui;

    QTcpSocket *_tcpSocket;
    quint16 _nextBlockSize;

    QString login;
    QString password;

    void authorizationAdmin();
    void authorizationUser();
};

#endif // MAINWINDOW_H
