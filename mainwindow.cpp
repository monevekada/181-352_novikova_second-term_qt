#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "server.h"
#include "admin.h"
#include "user.h"
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _nextBlockSize = 0;

    _tcpSocket = new QTcpSocket(this);
    _tcpSocket->connectToHost("localhost", 8080);

    connect(_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
}

void MainWindow::slotReadyRead(){
    QDataStream in(_tcpSocket);

    while(true){
        if (_nextBlockSize == 0){
            if (_tcpSocket->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                break;
            }
            in >> _nextBlockSize;
        }

        if (_tcpSocket->bytesAvailable() < _nextBlockSize){
            break;
        }

        QString str;
        in >> str;

        if (str == "auth:failed") {
            authorizationFailed();
        }
        if (str == "auth:admin") {
            authorizationAdmin();
        }
        if (str == "auth:user") {
            authorizationUser();
        }

        _nextBlockSize = 0;
    }
}

void MainWindow::slotSendToServer(){

    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    login = ui->lineEditLogin->text();
    password = ui->lineEditPassword->text();

    QString str = "auth:" + login + ":" + password;

    out << quint16(0) << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    _tcpSocket->write(arrBlock);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::authorizationFailed()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Authorization");

    msgBox.setText("Error!");
    msgBox.setInformativeText("Your login or password is incorrect. Do you want to retry?");
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setStandardButtons(QMessageBox::Retry | QMessageBox::Close);
    msgBox.setDefaultButton(QMessageBox::Retry);

    if(msgBox.exec() == QMessageBox::Retry)
    {
        msgBox.close();
    }
    else
    {
       QApplication::quit();
    }
}

void MainWindow::authorizationAdmin() {
    close();
    Admin * wind = new Admin(_tcpSocket, _tcpSocket->socketDescriptor());
    wind->setModal(true);
    wind->exec();
}

void MainWindow::authorizationUser()
{
    close();
    User * wind = new User(_tcpSocket, _tcpSocket->socketDescriptor());
    wind->setModal(true);
    wind->exec();
}

void MainWindow::on_pushButtonOK_clicked()
{
    if (ui->lineEditLogin->isModified() && ui->lineEditPassword->isModified())
    {
        slotSendToServer();
    }
}
