#ifndef USER_H
#define USER_H

#include <QDialog>
#include <QTcpSocket>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/evp.h>
#include "viewer.h"

namespace Ui {
class User;
}

class User : public QDialog
{
    Q_OBJECT

private:
    Ui::User *ui;
    Viewer *view;

    QTcpSocket *_tcpSocket;
    quint64 _nextBlockSize;

    QString encrypt(QString plainText, int do_encrypt); // 0 - decrypt, 1 - encrypt

public:
    User(QTcpSocket *mainSocket, qintptr descriptor);
    ~User();

signals:

public slots:
    void slotReadyRead();
    void slotSendToServer(const QString &str);
private slots:
    void on_pushButtonSR_clicked();
    void on_pushButtonFind_clicked();
};

#endif // USER_H
