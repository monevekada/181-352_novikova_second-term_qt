#include "mainwindow.h"
#include <QApplication>
#include "server.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w; // клиент
    w.show();

    Server server(8080); // сервер + база

    return a.exec();
}
